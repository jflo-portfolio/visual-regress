import os
import time

import pytest

from tests.integration import test_integration
from visual_regress import browser, cli


@pytest.fixture
def mock_function(monkeypatch):
    """fixture used to test cli"""

    def mock_blank(*args):
        time.sleep(1)
        return

    monkeypatch.setattr(browser.Browser, "__init__", mock_blank)
    monkeypatch.setattr(browser.Browser, "quit", mock_blank)
    monkeypatch.setattr(os, "makedirs", mock_blank)
    monkeypatch.setattr(cli, "process_diffs", mock_blank)
    monkeypatch.setattr(cli, "process_screenshots", mock_blank)
    monkeypatch.setattr(cli, "process_html", mock_blank)
    monkeypatch.setattr(cli, "optimize_imgs", mock_blank)


@pytest.mark.usefixtures("mock_function")
def test_cli_unit():
    """tests the visual-regress cli

    This is the same as the integration test,
    except this uses fixtures
    """
    test_integration.test_cli()
