import sys

from visual_regress import cli


def test_cli():
    """test the visual-regress cli"""
    sys.argv = [
        "visual-regress",
        "tests/sample_data/test.yml",
        "--output",
        "tests/sample_data/screenshots",
    ]
    cli.main()
