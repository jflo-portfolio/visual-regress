.PHONY: test-unit test-integration test test-all lint format default
.DEFAULT_GOAL := default

test-unit:
	pytest -vs tests/unit

test-integration:
	pytest -vs tests/integration

test: test-unit

test-all:
	test
	test-integration

lint:
	flake8 .
	pydocstyle .

format:
	black .
	isort .

default:
	@echo "visual-regress"
