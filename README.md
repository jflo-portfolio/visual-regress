# visual-regress

visual-regress is a Python 3 command line tool that will generate screenshots to be used for visual regression testing.

## Requirements
You will need Python 3 installed on your system.
This tool uses [Selenium], so you will need to download either the
[ChromeDriver] or [geckodriver] and into your system path.

[Selenium]: http://www.seleniumhq.org/
[ChromeDriver]: https://sites.google.com/a/chromium.org/chromedriver/downloads
[geckodriver]: https://github.com/mozilla/geckodriver/releases


## To Install
pip install git+https://bitbucket.org/jflo-portfolio/visual-regress.git
After installing you should have the visual-regress command line program available to you.

## Usage

You will first need to create a YAML file specifying the urls and screen sizes.

Example YAML file:


```
- name: Homepage
    url1: http://qa.nyu.edu/
    url2: http://www.nyu.edu/
    screen_sizes:
    - 320x2500
    - 480x2500
    - 690x2500
    - 930x2500
    - 1200x2500

- name: Channel
    url1: http://qa.nyu.edu/about.html
    url2: http://www.nyu.edu/about.html
    screen_sizes:
    - 320x2500
    - 480x2500
    - 690x2500
    - 930x2500
    - 1200x2500
```

The command:
```visual-regress data.yml``` will create a directory named "screenshots". In this directory, you will find an index.html file.

When you open this file in browser you should see something like this:

![alt text](visual_regress.png "visual-regress screenshot")


The first image is a screenshot of url1. The second is a screenshot of url2.
The third image is the diff of url1 and url2.


## Other options

The -b/--browser flag allows you to specify which browser to use.
It will use Chrome by default, the only other option is Firefox.  

The -o/--output flag specifies the name of directory for the screenshots.
The default is a directory named 'screenshots'.

The -r/--remote-url flag specifies a url for a remote selenium driver.

**Examples:**

Create screenshots in Chrome:  

```visual-regress data.yml --browser chrome --output screenshots/chrome_screenshots```

Create screenshots in Firefox:

```visual-regress data.yml --browser firefox --output screenshots/firefox_screenshots```
