import argparse
import glob
import os
from multiprocessing import Pool
from pathlib import Path

from jinja2 import Environment, PackageLoader, select_autoescape
from PIL import Image, ImageChops, ImageFile
from rich.console import Console
from slugify import slugify
from yaml import Loader, YAMLError, load

from .browser import Browser

env = Environment(
    loader=PackageLoader("visual_regress"), autoescape=select_autoescape()
)

ImageFile.LOAD_TRUNCATED_IMAGES = True


def screenshot(driver, screen_size, name, url, output="./screenshots"):
    """Use selenium to create a browser screenshot

    :param driver: browser, defaults to None
    :type driver: selenium browser object, optional
    :param screen_size: screen size
    :type screen_size: str
    :param name: base name of the file
    :type name: str
    :param url: url to take the screenshot from
    :type url: str
    :param output: directory to put the screenshots,
     defaults to "./screenshots"
    :type output: str, optional
    """
    driver.resize(screen_size)
    driver.get(url)
    slug = slugify(name)
    img_name = "{0}/imgs/{1}_{2}.png".format(output, slug, screen_size)
    driver.screenshot(img_name)


def process_screenshots(driver, data, output="./screenshots"):
    """Process screenshots

    creates screenshots from the information specified in the yaml file

    :param driver: browser
    :type driver: selenium browser object, optional
    :param data: dictionary created from the yaml file
    :type data: dict, optional
    :param output: path to save the screenshots, defaults to "./screenshots"
    :type output: str, optional
    """
    for page in data:
        name = page.get("name")
        url1 = page.get("url1")
        url2 = page.get("url2")
        screen_sizes = page.get("screen_sizes")

        for screen_size in screen_sizes:
            screenshot(driver, screen_size, name + "-1", url1, output)
            screenshot(driver, screen_size, name + "-2", url2, output)


def diff_images(img1, img2, diff_img):
    """Get the difference between 2 images.

    :param img1: path of the first image
    :type img1: str
    :param img2: path of the second image
    :type img2: str
    :param diff_img: path of the file to save the differences
    :type diff_img: str
    """
    image_one = Image.open(img1).convert("RGB")
    image_two = Image.open(img2).convert("RGB")

    diff = ImageChops.difference(image_one, image_two)

    if diff.getbbox():
        diff = diff.convert("RGB")
        diff.save(diff_img)
    else:
        # if no difference, create a small transparent image
        image = Image.new("RGBA", (1, 1), (255, 0, 0, 0))
        image.save(diff_img)


def diff_image_worker(img_tuple):
    """Worker function for diff_images()

    This function is used in a multiprocessing pool
    to run diff_images() in parallel
    :param img_tuple: tuple (img1, img2, diff_img)
    :type img_tuple: tuple
    """
    diff_images(*img_tuple)


def process_diffs(data=None, output="./screenshots"):
    """Process image diff from the yaml file

    :param data: dictionary created from yaml file, defaults to None
    :type data: dict, optional
    :param output: path to images, defaults to "./screenshots"
    :type output: str, optional
    """
    images2diff = list()
    for page in data:
        name = page.get("name")
        slug = slugify(name)
        screen_sizes = page.get("screen_sizes")

        for screen_size in screen_sizes:
            fname1 = "{0}/imgs/{1}_{2}.png".format(
                output, slug + "-1", screen_size
            )
            fname2 = "{0}/imgs/{1}_{2}.png".format(
                output, slug + "-2", screen_size
            )
            diff_out = "{0}/imgs/{1}_{2}_{3}.png".format(
                output, slug, screen_size, "diff"
            )

            # print(fname1, fname2, diff_out)
            images2diff.append((fname1, fname2, diff_out))
    pool = Pool()
    pool.map(diff_image_worker, images2diff)


def resize_img(original_file, thumbnail_size, img_size, output_file):
    """creates a thumbnail and resized version of an image

    :param original_file: original file
    :type original_file: path
    :param thumbnail_size: size of thumbnail
    :type thumbnail_size: tuple
    :param img_size: size of main image
    :type img_size: tuple
    :param output_file: output file name
    :type output_file: str
    """
    img = Image.open(original_file)
    img.thumbnail(thumbnail_size, Image.BICUBIC)
    thumb_file = output_file + ".thumbnail.png"
    img.save(thumb_file, "PNG")

    img.thumbnail(img_size, Image.BICUBIC)
    img.save(output_file, "PNG")


def resize_worker(img_tuple):
    """worker function for resize_img()

    This function is used in a multiprocessing pool
    to run resize_img() in parallel
    :param img_tuple: tuple (original_file, thumbnail_size, img_size,
                             output_file)
    :type img_tuple: tuple
    """
    resize_img(*img_tuple)


def optimize_imgs(
    output="./screenshots", thumbnail_size=(320, 1920), img_size=(500, 920)
):
    """Optimize images

    Resizes the images located in the image directory
    :param output: image directory, defaults to "./screenshots"
    :type output: str, optional
    :param thumbnail_size: width, height, defaults to (320, 1920)
    :type thumbnail_size: tuple, optional
    :param img_size: width, height, defaults to (500, 1920)
    :type img_size: tuple, optional
    """
    file_list = list()
    for output_file in glob.glob(output + "/imgs/*.png"):
        original_file = "{0}".format(os.path.abspath(output_file))
        output_file = original_file
        file_list.append(
            (original_file, thumbnail_size, img_size, output_file)
        )
    pool = Pool()
    pool.map(resize_worker, file_list)


def process_html(data=None, output="./screenshots"):
    """Create html files

    :param data: dictionary created from yaml file, defaults to None
    :type data: dict, optional
    :param output: directory to put the html files, defaults to "./screenshots"
    :type output: str, optional
    """
    index_file = Path("{0}/index.html".format(output)).expanduser()
    tmpl_index = env.get_template("index.jinja")
    write_str = tmpl_index.render(data=data, slugify=slugify)
    with open(index_file, "w") as f:
        f.write(write_str)

    tmpl_screenshot = env.get_template("screenshot.jinja")
    for page in data:
        name = page.get("name")
        slug = slugify(name)
        for screen_size in page.get("screen_sizes"):
            screenshot_str = tmpl_screenshot.render(
                name=name, slug=slug, screen_size=screen_size, ptype="1"
            )
            with open(
                Path(
                    "{0}/screenshot_{1}_1_{2}.html".format(
                        output, slug, screen_size
                    )
                ).expanduser(),
                "w",
            ) as f:
                f.write(screenshot_str)

            screenshot_str = tmpl_screenshot.render(
                name=name, slug=slug, screen_size=screen_size, ptype="2"
            )
            with open(
                Path(
                    "{0}/screenshot_{1}_2_{2}.html".format(
                        output, slug, screen_size
                    )
                ).expanduser(),
                "w",
            ) as f:
                f.write(screenshot_str)

            screenshot_str = tmpl_screenshot.render(
                name=name, slug=slug, screen_size=screen_size, ptype="diff"
            )
            with open(
                Path(
                    "{0}/screenshot_{1}_{2}_diff.html".format(
                        output, slug, screen_size
                    )
                ).expanduser(),
                "w",
            ) as f:
                f.write(screenshot_str)


def main():
    """visual-regress

    script that generates screenshots
    from 2 different versions
    of a site (for example QA and PROD) and shows the differences.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("yaml_file", help="YAML file")
    parser.add_argument(
        "-o",
        "--output",
        default="./screenshots",
        action="store",
        help="Output directory",
    )
    parser.add_argument(
        "-b",
        "--browser",
        default="Chrome",
        action="store",
        help="Browser, chrome or firefox",
    )
    parser.add_argument(
        "-r",
        "--remote-url",
        default=None,
        action="store",
        help="Specify remote selenium driver",
    )
    args = parser.parse_args()

    output = args.output
    yaml_file = args.yaml_file
    browser = args.browser
    remote_url = args.remote_url

    console = Console()
    tasks = True

    with console.status("[bold green]Working..."):
        console.log("yaml_file: {}".format(yaml_file))
        console.log("output: {}".format(output))
        console.log("browser: {}".format(browser))
        console.log("remote_url: {}".format(remote_url))
        if not (os.path.exists(output) and os.path.isdir(output)):
            os.makedirs(output)
            os.makedirs(output + "/imgs")
        while tasks:
            console.log("setting up driver")
            driver = Browser(browser, remote_url)
            with open(yaml_file, "r") as fh:
                try:
                    console.log("creating screenshots")
                    data = load(fh, Loader=Loader)
                    process_screenshots(driver, data, output)

                    console.log("processing image diffs")
                    process_diffs(data, output)

                    console.log("re-sizing images")
                    optimize_imgs(output)

                    console.log("creating html")
                    process_html(data, output)

                    console.log("done")

                    tasks = False
                except YAMLError as exc:
                    print(exc)
            driver.quit()


if __name__ == "__main__":
    main()
