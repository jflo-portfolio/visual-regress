from selenium import webdriver


class Browser:
    """Represent a web browser using selenium."""

    def __init__(self, browser="Chrome", remote_url=None):
        """Create a Browser object."""
        if browser.lower() == "firefox":
            options = webdriver.firefox.options.Options()
            options.headless = True
            self.driver = webdriver.Firefox(options=options)
        else:
            options = webdriver.chrome.options.Options()
            options.add_argument("--headless")
            options.add_argument("--disable-gpu")
            if remote_url:
                self.driver = webdriver.Remote(
                    remote_url, desired_capabilities=options.to_capabilities()
                )
            else:
                self.driver = webdriver.Chrome(options=options)

        self.driver.implicitly_wait(10)

    def get(self, url):
        """Get url."""
        self.driver.get(url)

    def resize(self, screen_size):
        """Resize image."""
        width, height = screen_size.split("x")
        self.driver.set_window_size(width, height)

    def screenshot(self, out_file):
        """Take a screenshot."""
        self.driver.save_screenshot(out_file)

    def quit(self):
        """Quit browser."""
        self.driver.quit()
